FROM ubuntu:latest
RUN apt update && apt install -y fio

RUN mkdir /home/capa1
RUN mkdir /home/capa2
RUN mkdir /home/capa3
RUN mkdir /home/capa4
RUN mkdir /home/capa5
RUN mkdir /home/capa6 && echo "Archivo" > /home/capa6/txt_capa6.txt

fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=/home/capa6/txt_capa6 --bs=4k --iodepth=64 --size=1G --readwrite=randrw --rwmixread=75
